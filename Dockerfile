FROM docker.io/opensearchproject/opensearch:2.11.0

COPY opensearch.yml.org /usr/share/opensearch/config/opensearch.yml

# Save the files for copying into a writeable folder
USER 0
RUN mkdir -p /var/cache/opensearch  &&\
    cp -r /usr/share/opensearch/config /var/cache/opensearch/ &&\
    chown -R 1000:1000 /var/cache/opensearch &&\
    chmod a+r /usr/share/opensearch/config/opensearch.yml &&\
    chmod -R a+rx  /usr/share/opensearch /var/cache/opensearch/config
USER 1000

ENV discovery.type        single-node \
    bootstrap.memory_lock true \
    OPENSEARCH_JAVA_OPTS  -Xms4300m -Xmx4300m

ENTRYPOINT cp -r /var/cache/opensearch/config/* /usr/share/opensearch/config/ && ./opensearch-docker-entrypoint.sh
